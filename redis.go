package main

import (
	"io"

	"bitbucket.org/_metalogic_/log"
	"github.com/go-redis/redis/v7"
)

const (
	maxRetries = 5
)

// initRedis initializes the Redis client for listening to logspout messages
func initRedis(redisHost string) *redis.Client {
	opt := &redis.Options{Addr: redisHost}
	return redis.NewClient(opt)
}

func dequeue(rdb *redis.Client, queue string, w io.Writer) {
	for {
		// BLPOP blocks until something arrives on the queue
		// use `rdb.BLPop(0, "queue")` for infinite waiting time
		result, err := rdb.BLPop(0, queue).Result()
		if err != nil {
			log.Fatal(err)
		}

		// result[0] is the name of queue, result[1] is the value
		v := []byte(result[1])
		log.Tracef("popped record %s: %s", result[0], result[1])

		_, err = w.Write(v)
		if err != nil {
			log.Error(err.Error())
		}
	}
}
