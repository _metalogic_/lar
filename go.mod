module bitbucket.org/_metalogic_/lar

go 1.15

require (
	bitbucket.org/_metalogic_/config v1.4.0
	bitbucket.org/_metalogic_/flag v1.0.1
	bitbucket.org/_metalogic_/keygen v1.0.0
	bitbucket.org/_metalogic_/log v1.4.1
	github.com/go-redis/redis/v7 v7.1.0
	github.com/gorilla/websocket v1.4.1
)

require (
	github.com/dgraph-io/badger/v3 v3.2103.2
	gopkg.in/yaml.v2 v2.2.7 // indirect
)
