// Copyright 2013 The Gorilla WebSocket Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	"bytes"
	"fmt"
	"io"
	"strconv"
	"time"

	"bitbucket.org/_metalogic_/keygen"
	"bitbucket.org/_metalogic_/log"
	badger "github.com/dgraph-io/badger/v3"
)

// a singleton source of monotonically increasing keys based on the current time
var keys *keygen.Keyseq

func init() {
	keys = keygen.GetKeyseq()
}

// Hub manages writes of log records to the Badger timeseries database, streaming log records
// to active clients, and executing client queries against the timeseries
type Hub struct {
	// Badger timeseries database
	badgerDB *badger.DB

	// Badger keys time-to-live
	badgerTTL time.Duration

	// Registered clients.
	clients map[*Client]bool

	// Hub writes messages from the Redis queue on stream
	stream chan []byte

	// Register requests from the clients.
	register chan *Client

	// Unregister requests from clients.
	unregister chan *Client
}

func newHub(db *badger.DB, ttl time.Duration) *Hub {
	return &Hub{
		badgerDB:   db,
		badgerTTL:  ttl,
		stream:     make(chan []byte, streamSize),
		register:   make(chan *Client),
		unregister: make(chan *Client),
		clients:    make(map[*Client]bool),
	}
}

func (h *Hub) run() {
	for {
		select {
		case client := <-h.register:
			h.clients[client] = true
			log.Debugf("registered client '%v'", client)
		case client := <-h.unregister:
			if _, ok := h.clients[client]; ok {
				delete(h.clients, client)
				close(client.send)
				log.Debugf("unregistered client '%v'", client)
			}
		case message := <-h.stream:
			for client := range h.clients {
				if client.stream {
					select {
					case client.send <- message:
					default:
						close(client.send)
						delete(h.clients, client)
					}
				}
			}
		}
	}
}

func (h *Hub) hasListeners() bool {
	for client := range h.clients {
		if client.stream {
			return true
		}
	}
	return false
}

// query Badger for log records between start and end timestamps, writing records to client channel;
// maximimum number of records returned is controlled by limit; if limit is zero there is no limit
func (h *Hub) query(start, end int64, limit int, client *Client) {
	var (
		from []byte
		to   []byte
	)
	from = []byte(strconv.FormatInt(start, 10))
	if end != 0 {
		to = []byte(strconv.FormatInt(end, 10))
	}

	err := h.badgerDB.View(func(txn *badger.Txn) error {
		opts := badger.DefaultIteratorOptions
		opts.PrefetchValues = false
		it := txn.NewIterator(opts)
		defer it.Close()
		var count = 0
		for it.Seek(from); it.Valid(); it.Next() {
			count++
			if limit != 0 && count > limit {
				break
			}
			item := it.Item()
			k := item.Key()
			if end != 0 && bytes.Compare(k, to) > 0 {
				break
			}
			err := item.Value(func(v []byte) error {
				log.Tracef("sending %s", fmt.Sprintf("{\"time\": %s, \"record\": %s}", k, v))
				client.send <- []byte(fmt.Sprintf("{\"time\": %s, \"record\": %s}", k, v))
				return nil
			})
			if err != nil {
				return err
			}
		}
		// return EOF when no more query results
		return io.EOF
	})
	if err != nil {
		log.Tracef("sending %s", err.Error())
		client.send <- []byte(err.Error())
	}
}

func (h *Hub) Write(data []byte) (n int, err error) {
	key := keys.Next()
	err = h.badgerDB.Update(func(txn *badger.Txn) error {
		// write data to Badger timeseries
		entry := badger.NewEntry(key, data).WithTTL(h.badgerTTL)
		err := txn.SetEntry(entry)
		return err
	})

	if err != nil {
		return 0, fmt.Errorf("Badger failed saving key '%s', value (%s): %s", string(key), string(data), err)
	}

	// send data to each active listener
	if h.hasListeners() {
		record := []byte(fmt.Sprintf("{\"time\": %s, \"record\": %s}", key, data))
		h.stream <- record
	}
	return len(data), nil
}
