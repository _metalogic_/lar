package main

import (
	_ "expvar"
	"strings"

	"fmt"
	"net/http"
	"os"
	"os/signal"
	"time"

	"bitbucket.org/_metalogic_/config"
	"bitbucket.org/_metalogic_/flag"
	"bitbucket.org/_metalogic_/log"
	"github.com/dgraph-io/badger/v3"
	"github.com/go-redis/redis/v7"
)

var (
	build          string
	version        string
	username       string
	password       string
	redisHost      string
	streamSize     int
	queue          string
	dir            string
	backupFreq     time.Duration
	badgerTTL      time.Duration
	compactionFreq time.Duration
	flush          bool
	err            error
	levelFlg       log.Level
	truncateFlg    bool
	bdb            *badger.DB
	rdb            *redis.Client
)

const (
	listenPort = ":8080"
	localhost  = "localhost:8080"
	localip    = "127.0.0.1:8080"
)

func init() {

	username = config.MustGetConfig("BASIC_AUTH_USERNAME")
	password = config.MustGetConfig("BASIC_AUTH_PASSWORD")

	backupFreq = config.IfGetDuration("LOG_BACKUP_FREQUENCY", 0)
	// default time-to-live for keys is 90 days
	badgerTTL = config.IfGetDuration("BADGER_KEYS_TTL", 90*24*time.Hour)
	compactionFreq = config.IfGetDuration("BADGER_COMPACTION_FREQUENCY", 10*50*time.Second)

	flag.StringVar(&queue, "queue", "logspout", "redis listen queue")
	flag.StringVar(&dir, "badger", "/data/badger", "path to Badger data directory")
	flag.IntVar(&streamSize, "stream-size", 256, "length of the stream channel buffer")
	flag.BoolVar(&truncateFlg, "truncate", true, "truncate log files at start up")

	flag.Var(&levelFlg, "level", "set level of logging by the API")
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "\n%s\n\n", info())
		flag.PrintDefaults()
	}
}

func info() string {
	// TODO: build a better version string
	return fmt.Sprintf("lar Version %s, Build [Git HEAD %s]", version, build)
}

func main() {
	flag.Parse()

	var err error
	// if log level not set on command line get it from environment; default level is DEBUG
	if levelFlg == log.None {
		level := config.IfGetenv("LOG_LEVEL", "DEBUG")
		err = levelFlg.Set(level)
		if err != nil {
			log.Errorf("invalid log level '%s' - shouldn't happen", level)
			levelFlg = log.DebugLevel
		}
	}
	log.SetLevel(levelFlg)

	log.Info(info())

	if backupFreq != 0 {
		log.Debugf("log backup frequency: %s", backupFreq)
	} else {
		log.Warning("log backups are disabled")
	}

	// init Badger database
	// bdb, err := badger.Open(badger.DefaultOptions(dir))

	// init Badger database - do not memory map log files to avoid OOM fails
	bdb, err := badger.Open(
		badger.DefaultOptions(dir))

	if err != nil {
		log.Fatal(err)
	}
	defer bdb.Close()

	// run scheduled Badger backups under Badger dir; forces exit on error
	if backupFreq != 0 {
		go backup(bdb, dir, backupFreq)
	}

	if compactionFreq != 0 {
		go compact(bdb, compactionFreq)
	}

	hub := newHub(bdb, badgerTTL)

	host := config.MustGetConfig("REDIS_HOST")
	rdb = initRedis(host)

	// read log messages from redis, writing to Hub (and thence to Badger)
	go dequeue(rdb, queue, hub)

	// serve WebSocket requests from clients to tail the log stream or query the log history
	serve(hub)
}

func serve(hub *Hub) {
	go hub.run()
	http.HandleFunc("/", serveHome)
	http.HandleFunc("/ws", func(w http.ResponseWriter, r *http.Request) {
		// require secure connections except for public hosts
		if r.Host != localhost && r.Host != localip && !strings.Contains(r.Host, "-private") {
			http.Error(w, "Forbidden", http.StatusForbidden)
			return
		}
		serveWS(hub, w, r)
	})
	http.HandleFunc("/secure", BasicAuth(serveSecure))
	http.HandleFunc("/wss", func(w http.ResponseWriter, r *http.Request) {
		serveWS(hub, w, r)
	})

	http.HandleFunc("/health", serveHealth)

	log.Fatalf("ListenAndServe: %s", http.ListenAndServe(listenPort, nil))
}

func serveHome(w http.ResponseWriter, r *http.Request) {
	log.Debug(r.URL)
	if r.URL.Path != "/" {
		http.Error(w, "Not found", http.StatusNotFound)
		return
	}
	if r.Method != "GET" {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}
	http.ServeFile(w, r, "home.html")
}

func serveSecure(w http.ResponseWriter, r *http.Request) {
	log.Debug(r.URL)
	if r.URL.Path != "/secure" {
		http.Error(w, "Not found", http.StatusNotFound)
		return
	}
	if r.Method != "GET" {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}
	http.ServeFile(w, r, "secure.html")
}

func serveHealth(w http.ResponseWriter, r *http.Request) {
	log.Debug(r.URL)
	if r.URL.Path != "/health" {
		http.Error(w, "Not found", http.StatusNotFound)
		return
	}
	if r.Method != "GET" {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}

	// check Redis
	pong, err := rdb.Ping().Result()
	if err != nil {
		log.Fatal(err)
		return
	}

	log.Debugf("Redis ping: %s", pong)

	// TODO check Badger

	fmt.Fprintln(w, "ok")
}

// BasicAuth wraps a handler requiring HTTP basic auth for it using the given
// username and password and the specified realm, which shouldn't contain quotes.
func BasicAuth(fn http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		user, pass, _ := r.BasicAuth()
		if !check(user, pass) {
			w.Header().Set("WWW-Authenticate", "Basic realm=epbc")
			w.WriteHeader(401)
			w.Write([]byte("Unauthorized.\n"))
			return
		}
		fn(w, r)
	}
}

func check(u, p string) bool {
	if u == username && p == password {
		return true
	}
	return false
}

func awaitInterrupt() {
	sigCh := make(chan os.Signal)

	signal.Notify(sigCh, os.Interrupt)

	select {
	case <-sigCh:
		panic("Caught interrupt - exiting")
	}
}
