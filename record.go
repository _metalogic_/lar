package main

import "time"

// Record defines the fields in a log record
type Record struct {
	Timestamp time.Time `json:"@timestamp"`
	Host      string    `json:"host"`
	Docker    Docker    `json:"docker"`
	Message   string    `json:"message"`
	Event     *Event    `json:"event,omitempty"`
}

// Summary returns the summary form for a record
func (r Record) Summary() (summary Summary) {
	summary.Timestamp = r.Timestamp
	summary.ServiceName = r.Docker.Labels.ServiceName
	summary.Level = r.Event.Level
	summary.Message = r.Event.Message
	summary.Source = r.Event.Source
	return summary
}

// Summary defines a less detailed subset of record
type Summary struct {
	Timestamp   time.Time `json:"@timestamp"`
	ServiceName string    `json:"service-name"`
	Level       string    `json:"level"`
	Message     string    `json:"msg"`
	Source      Source    `json:"source"`
}

// Docker defines the fields of the Docker object of a log record
type Docker struct {
	Name     string `json:"name"`
	CID      string `json:"cid"`
	Image    string `json:"image"`
	ImageTag string `json:"image_tag"`
	Source   string `json:"source"`
	Labels   Labels `json:"labels"`
}

// Labels defines the labels in a Docker object
type Labels struct {
	NameSpace   string `json:"com.docker.stack.namespace"`
	NodeID      string `json:"com.docker.swarm.node.id"`
	ServiceID   string `json:"com.docker.swarm.service.id"`
	ServiceName string `json:"com.docker.swarm.service.name"`
	Task        string `json:"com.docker.swarm.task,omitempty"`
	TaskID      string `json:"com.docker.swarm.task.id"`
	TaskName    string `json:"com.docker.swarm.task.name"`
}

// Event defines the fields of the Event object of a log record
type Event struct {
	Level   string `json:"level"`
	Message string `json:"msg"`
	Source  Source `json:"source"`
}

// Source defines the fields of the Source object of an Event object
type Source struct {
	File string `json:"file"`
	Line int    `json:"line"`
}
