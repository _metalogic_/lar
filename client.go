// Copyright 2013 The Gorilla WebSocket Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	"bytes"
	"fmt"
	"net/http"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/_metalogic_/log"
	"github.com/gorilla/websocket"
)

const (
	// Time allowed to write a message to the peer.
	writeWait = 10 * time.Second

	// Time allowed to read the next pong message from the peer.
	pongWait = 60 * time.Second

	// Send pings to peer with this period. Must be less than pongWait.
	pingPeriod = (pongWait * 9) / 10

	// Maximum message size allowed from peer.
	maxMessageSize = 512
)

var (
	newline = []byte{'\n'}
	space   = []byte{' '}
	limit   = []byte("limit")
	query   = []byte("query")
	start   = []byte("stream start")
	stop    = []byte("stream stop")
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

// Client is a middleman between the websocket connection and the hub.
type Client struct {
	hub *Hub

	// Send message stream to clientchannel
	stream bool

	// The websocket connection.
	conn *websocket.Conn

	// Buffered channel of outbound messages.
	send chan []byte

	// limit the number of records in a query response; 0 means no limit
	limit int
}

// readPump pumps messages from the websocket connection to the hub.
//
// The application runs readPump in a per-connection goroutine. The application
// ensures that there is at most one reader on a connection by executing all
// reads from this goroutine.
func (c *Client) readPump() {
	defer func() {
		c.hub.unregister <- c
		c.conn.Close()
	}()
	c.conn.SetReadLimit(maxMessageSize)
	c.conn.SetReadDeadline(time.Now().Add(pongWait))
	c.conn.SetPongHandler(func(string) error { c.conn.SetReadDeadline(time.Now().Add(pongWait)); return nil })
	for {
		_, message, err := c.conn.ReadMessage()
		if err != nil {
			if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
				log.Error(err)
			}
			break
		}
		message = bytes.TrimSpace(bytes.Replace(message, newline, space, -1))
		if bytes.Equal(message, start) {
			log.Debug("stream start")
			c.stream = true
			continue
		}
		if bytes.Equal(message, stop) {
			log.Debug("stream stop")
			c.stream = false
			continue
		}

		if len(message) >= len(limit) && bytes.Equal(message[0:5], limit) {
			c.stream = false
			limit, err := parseLimit(message[5:])
			if err != nil {
				log.Error(err)
				continue
			}
			log.Debugf("limit query results to %d", limit)

			c.limit = limit

			continue
		}

		if len(message) >= len(query) && bytes.Equal(message[0:5], query) {
			c.stream = false
			start, end, err := parseQuery(message[5:])
			if err != nil {
				log.Error(err)
				continue
			}
			log.Debugf("query from %d to %d", start, end)

			c.hub.query(start, end, c.limit, c)
			continue
		}

	}
}

func parseQuery(r []byte) (start, end int64, err error) {
	// queries expect durations in nanoseconds and times in nanoseconds since the Unix epoch
	// clients should provide interfaces that convert from human-friendly time and durations formats
	args := strings.Fields(string(bytes.TrimSpace(r)))
	if len(args) == 0 || len(args) > 3 {
		return start, end, fmt.Errorf("query: %v", args)
	}
	log.Debugf("query: %v", args)
	var now = time.Now().UnixNano()
	op := args[0]
	switch op {
	case "since":
		if len(args[1:]) != 1 {
			return start, end, fmt.Errorf("query since: invalid duration %v", args[1:])
		}
		since, err := strconv.ParseInt(args[1], 10, 64)
		if err != nil {
			return start, end, fmt.Errorf("query since: invalid duration '%v'", args[1])
		}
		return now - since, 0, nil
	case "from":
		if len(args[1:]) == 1 { // from start
			start, err = strconv.ParseInt(args[1], 10, 64)
			if err != nil {
				return start, end, fmt.Errorf("query from: invalid start time '%v'", args[1])
			}
		}
		if len(args[1:]) == 2 { // from start end
			start, err = strconv.ParseInt(args[1], 10, 64)
			if err != nil {
				return start, end, fmt.Errorf("query from: invalid start time '%v'", args[1])
			}
			end, err = strconv.ParseInt(args[2], 10, 64)
			if err != nil {
				return start, end, fmt.Errorf("query from: invalid end time '%v'", args[2])
			}
		}
		if len(args[1:]) != 1 && len(args[1:]) != 2 {
			return start, end, fmt.Errorf("query from: invalid range %v", args[1:])
		}
		if end != 0 && end < start {
			return start, end, fmt.Errorf("query from: invalid range; end time %d is before start time %d", end, start)
		}
		return start, end, nil
	case "window":
		var timestamp int64
		var window int64
		if len(args[1:]) != 2 {
			return start, end, fmt.Errorf("query window: requires timestamp and surround window '%v'", args[1:])
		}
		if len(args[1:]) == 2 { // window ts tw
			timestamp, err = strconv.ParseInt(args[1], 10, 64)
			if err != nil {
				return start, end, fmt.Errorf("query window: invalid timestamp '%v'", args[1])
			}
			window, err = strconv.ParseInt(args[2], 10, 64)
			if err != nil {
				return start, end, fmt.Errorf("query window: invalid window '%v'", args[2])
			}
		}
		start = timestamp - window
		end = timestamp + window
		return start, end, nil
	default:
		return start, end, fmt.Errorf("invalid range operator: found '%s' when expecting 'from' or 'since'", op)
	}
}

func parseLimit(r []byte) (limit int, err error) {
	args := strings.Fields(string(bytes.TrimSpace(r)))
	if len(args) != 1 {
		return limit, fmt.Errorf("invalid limit: %v", args)
	}
	log.Debugf("limit: %v", args)

	limit, err = strconv.Atoi(args[0])
	if err != nil {
		return limit, fmt.Errorf("invalid limit: %v: %s", args[0], err)
	}
	return limit, nil
}

// writePump pumps messages from the hub to the websocket connection.
//
// A goroutine running writePump is started for each connection. The
// application ensures that there is at most one writer to a connection by
// executing all writes from this goroutine.
func (c *Client) writePump() {
	ticker := time.NewTicker(pingPeriod)
	defer func() {
		ticker.Stop()
		c.conn.Close()
	}()
	for {
		select {
		case message, ok := <-c.send:
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if !ok {
				// The hub closed the channel.
				c.conn.WriteMessage(websocket.CloseMessage, []byte{})
				return
			}

			w, err := c.conn.NextWriter(websocket.TextMessage)
			if err != nil {
				return
			}

			w.Write(message)

			if err := w.Close(); err != nil {
				return
			}
		case <-ticker.C:
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if err := c.conn.WriteMessage(websocket.PingMessage, nil); err != nil {
				return
			}
		}
	}
}

// serveWS handles websocket requests from the peer.
func serveWS(hub *Hub, w http.ResponseWriter, r *http.Request) {
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Error(err)
		return
	}
	client := &Client{hub: hub, stream: false, conn: conn, send: make(chan []byte, streamSize)}
	client.hub.register <- client

	// Allow collection of memory referenced by the caller by doing all work in new goroutines.
	go client.writePump()
	go client.readPump()
}
