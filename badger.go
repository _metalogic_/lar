package main

import (
	"bufio"
	"fmt"
	"os"
	"path/filepath"
	"time"

	"bitbucket.org/_metalogic_/log"
	"github.com/dgraph-io/badger/v3"
)

func gc(db *badger.DB) {
	ticker := time.NewTicker(5 * time.Minute)
	defer ticker.Stop()
	for range ticker.C {
	again:
		err := db.RunValueLogGC(0.7)
		if err == nil {
			goto again
		}
	}
}

// backup Badger database every freq duration
func backup(db *badger.DB, dir string, freq time.Duration) {
	// create new backup directory as .../backups/<secs>.bak
	secs := time.Now().Unix()
	backups := filepath.Join(dir, "backups", fmt.Sprintf("%d", secs))
	err = os.MkdirAll(backups, os.ModePerm)
	if err != nil {
		log.Fatal(err)
	}

	var last uint64
	ticker := time.NewTicker(freq)
	defer ticker.Stop()
	for range ticker.C {
		path := filepath.Join(backups, fmt.Sprintf("%d.%s", last, "bak"))
		f, err := os.Create(path)
		if err != nil {
			log.Fatal(err)
		}
		defer f.Close()
		w := bufio.NewWriter(f)
		last, err = db.Backup(w, last)
		if err != nil {
			log.Fatal(err)
		}
		err = f.Close()
		if err != nil {
			log.Fatal(err)
		}
	}
}

func compact(db *badger.DB, freq time.Duration) {
	ticker := time.NewTicker(freq)
	defer ticker.Stop()
	for range ticker.C {
	again:
		err := db.RunValueLogGC(0.5)
		if err == nil {
			goto again
		}
	}
}
