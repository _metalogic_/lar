FROM golang:1.18 as builder

WORKDIR /build

COPY ./ /build

RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o command .

FROM metalogic/alpine:latest

RUN adduser -u 25010 -g 'Application Runner' -D runner

WORKDIR /home/runner

COPY --from=builder /build/command /usr/local/bin/lar
COPY --from=builder /build/home.html .
COPY --from=builder /build/secure.html .

RUN mkdir -p /data/badger && chown runner /data/badger

USER runner

CMD ["lar"]

HEALTHCHECK --interval=30s  --start-period=300s --timeout=30s --retries=10 CMD /usr/local/bin/health http://localhost:8080/health

